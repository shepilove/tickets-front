import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatDatepicker, MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatSelectModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import {ClientsComponent} from './components/clients/clients.component';
import {TicketsComponent} from './components/tickets/tickets.component';
import {HttpClientModule} from '@angular/common/http';
import {EditClientComponent} from './components/clients/edit-client/edit-client.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/login/login.component';
import {MainComponent} from './components/main/main.component';
import {AuthService} from './services/auth.service';
import {EditTicketComponent} from './components/tickets/edit-ticket/edit-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    TicketsComponent,
    EditClientComponent,
    LoginComponent,
    MainComponent,
    EditTicketComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents: [
    EditClientComponent,
    EditTicketComponent
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
