import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ClientModel} from '../components/clients/shared/client-model';

@Injectable()
export class AuthService implements CanActivate {

  private url = '/api';

  constructor(private router: Router,
              private http: HttpClient) {
  }

  canActivate() {
    if (this.getUser()) {
      return true;
    } else {
      this.router.navigate(['/login']);
    }
  }

  getUser(): ClientModel {
    // return JSON.parse(localStorage.getItem('user'));
    return new ClientModel();
  }

  login(login: string, password: string): Observable<any> {
    console.log(login);
    console.log(password);
    const headers = new HttpHeaders({login, password});
    return this.http.get(this.url, {headers});
  }

  register(client: ClientModel): Observable<any> {
    return this.http.post(this.url + '/register', client);
  }

  logout() {
    localStorage.removeItem('user');
    document.location.reload();
  }
}
