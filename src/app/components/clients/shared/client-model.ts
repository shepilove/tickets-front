export class ClientModel {
  id: number;
  login: string;
  password: string;
  firstName: string;
  lastName: string;
  surName: string;
  gender: string;
}
