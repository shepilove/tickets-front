import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ClientModel} from './client-model';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  private url = 'api/clients';

  constructor(private http: HttpClient) {
  }

  getAllClients(): Observable<any> {
    return this.http.get(this.url + '/getAll');
  }

  addNewClient(client: ClientModel): Observable<any> {
    return this.http.post(this.url + '/add', client);
  }

  editClient(client: ClientModel): Observable<any> {
    return this.http.put(this.url + '/update', client);
  }

  deleteClient(id: number): Observable<any> {
    return this.http.delete(this.url + '/delete/' + id);
  }
}
