import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent {

  clientForm = new FormGroup({
    login: new FormControl(this.data.client.login, []),
    password: new FormControl(this.data.client.password, []),
    surName: new FormControl(this.data.client.surName, []),
    firstName: new FormControl(this.data.client.firstName, []),
    lastName: new FormControl(this.data.client.lastName, []),
    gender: new FormControl(this.data.client.gender, [])
  });

  constructor(
    public dialogRef: MatDialogRef<EditClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  save() {
    if (this.clientForm.get('login').value && this.clientForm.get('surName').value &&
      this.clientForm.get('firstName').value && this.clientForm.get('lastName').value &&
      this.clientForm.get('password').value && this.clientForm.get('gender').value) {
      this.data.client.login = this.clientForm.get('login').value;
      this.data.client.surName = this.clientForm.get('surName').value;
      this.data.client.firstName = this.clientForm.get('firstName').value;
      this.data.client.lastName = this.clientForm.get('lastName').value;
      this.data.client.password = this.clientForm.get('password').value;
      this.data.client.gender = this.clientForm.get('gender').value;
      this.dialogRef.close(this.data.client);
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
