import {Component, OnInit} from '@angular/core';
import {ClientsService} from './shared/clients.service';
import {ClientModel} from './shared/client-model';
import {MatDialog} from '@angular/material';
import {EditClientComponent} from './edit-client/edit-client.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients: ClientModel[];
  displayedColumns: string[] = ['login', 'firstName', 'lastName', 'surName', 'gender', 'edit'];

  constructor(private clientService: ClientsService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    console.log('clients load');
    this.getAllClients();
  }

  getAllClients() {
    this.clientService.getAllClients().subscribe((result: any) => {
      this.clients = result;
    }, (error) => {
      console.log(error);
    });
  }

  edit(cl: ClientModel) {
    const dialogRef = this.dialog.open(EditClientComponent, {
      width: '450px',
      data: {client: cl, register: false}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.clientService.editClient(data).subscribe((result: any) => {
          if (result.id) {
            this.getAllClients();
          }
        }, (error) => {
          alert(error.error);
        });
      }
    }, (error) => {
      alert(error.error);
    });
  }

  remove(id: number) {
    this.clientService.deleteClient(id).subscribe((result: any) => {
      console.log(result);
      this.getAllClients();
    }, (error) => {
      alert(error.error);
    });
  }

  addNewClient() {
    const dialogRef = this.dialog.open(EditClientComponent, {
      width: '450px',
      data: {client: new ClientModel(), register: false}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.clientService.addNewClient(data).subscribe((result: any) => {
          if (result.id) {
            this.getAllClients();
          }
        }, (error) => {
          alert(error.error);
        });
      }
    });
  }
}
