import {Component, OnInit} from '@angular/core';
import {TicketModel} from './shared/ticket-model';
import {TicketsService} from './shared/tickets.service';
import {MatDialog} from '@angular/material';
import {EditTicketComponent} from './edit-ticket/edit-ticket.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  tickets: TicketModel[];
  displayedColumns: string[] = ['date', 'cost', 'client', 'edit'];

  constructor(private ticketService: TicketsService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    console.log('tickets load');
    this.getAllTickets();
  }

  getAllTickets() {
    this.ticketService.getAllTickets().subscribe((result: any) => {
      this.tickets = result;
    }, (error) => {
      console.log(error);
    });
  }

  remove(id: number) {
    this.ticketService.deleteTicket(id).subscribe((result: any) => {
      console.log(result);
      this.getAllTickets();
    }, (error) => {
      alert(error.error);
    });
  }

  addNewTicket() {
    const dialogRef = this.dialog.open(EditTicketComponent, {
      width: '450px',
      data: new TicketModel()
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.ticketService.addNewTicket(data).subscribe((result: any) => {
          if (result.id) {
            this.getAllTickets();
          }
        }, (error) => {
          alert(error.error);
        });
      }
    });
  }
}
