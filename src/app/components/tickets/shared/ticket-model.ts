import {ClientModel} from '../../clients/shared/client-model';

export class TicketModel {
  id: number;
  cost: number;
  date: Date;
  client: ClientModel;
}
