import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TicketModel} from './ticket-model';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private url = 'api/tickets';

  constructor(private http: HttpClient) {
  }

  getAllTickets(): Observable<any> {
    return this.http.get(this.url + '/getAll');
  }

  addNewTicket(ticket: TicketModel): Observable<any> {
    return this.http.post(this.url + '/add', ticket);
  }

  editTicket(ticket: TicketModel): Observable<any> {
    return this.http.put(this.url + '/update', ticket);
  }

  deleteTicket(id: number): Observable<any> {
    return this.http.delete(this.url + '/delete/' + id);
  }
}
