import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TicketModel} from '../shared/ticket-model';
import {ClientsService} from '../../clients/shared/clients.service';
import {ClientModel} from '../../clients/shared/client-model';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  clients: ClientModel[];

  constructor(
    public dialogRef: MatDialogRef<EditTicketComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TicketModel, private clientService: ClientsService) {
  }

  ticketForm = new FormGroup({
    date: new FormControl(this.data.date, []),
    cost: new FormControl(this.data.cost, []),
    client: new FormControl(this.data.client, [])
  });

  ngOnInit() {
    this.clientService.getAllClients().subscribe((result: any) => {
      this.clients = result;
      console.log(result);
    }, (error) => {
      console.log(error.error);
    });
  }

  save() {
    if (this.ticketForm.get('date').value && this.ticketForm.get('cost').value &&
      this.ticketForm.get('client').value) {
      this.data.date = this.ticketForm.get('date').value;
      this.data.cost = this.ticketForm.get('cost').value;
      this.data.client = this.ticketForm.get('client').value;
      this.dialogRef.close(this.data);
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
