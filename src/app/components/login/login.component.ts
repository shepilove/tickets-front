import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {EditClientComponent} from '../clients/edit-client/edit-client.component';
import {ClientModel} from '../clients/shared/client-model';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl = '/';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog
  ) {
    if (this.authService.getUser()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.loginForm.controls.login.value, this.loginForm.controls.password.value).subscribe(
      data => {
        console.log(data);
        this.router.navigate([this.returnUrl]);
      },
      error => {
        console.log(error);
      });
  }

  register() {
    const dialogRef = this.dialog.open(EditClientComponent, {
      width: '450px',
      data: {client: new ClientModel(), register: true}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.authService.register(result).subscribe((data: ClientModel) => {
          if (data.id) {
            alert('Пользователь зарегистрирован!');
            localStorage.setItem('user', JSON.stringify(data));
            this.router.navigate([this.returnUrl]);
          } else {
            alert('Проблемы при регистрации пользователя!');
            return;
          }
        }, error => {
          alert(error.error);
        });
      }
    });
  }
}
